# Pendu
## Bouttons par lettre
- alphabet
- si on clique sur un boutton
 - il se desactive
 - on sauvegarde le clique
 - modifier le compteur de vie (si lettre fausse)
 - desactiver tout les bouttons si on a perdu / gagné
## Compteur de vies
- savoir combien de vies on à
- afficher un symbole par vies
- evolue au cours de la partie
- afficher perdu (si on a perdu)
    - une fonction pour determiner si j'ai perdu
- afficher gagné (si on a gagné)
    - une fonction pour determiner si j'ai gagné
## Afficher le mot
- liste de mots
- choisir un mot aléatoire (au début de la partie)
- afficher le mot en caché
  - dertminer si chaque lettre est trouvée ou non
  - afficher le mot en entier si j'ai gagné ou perdu
 ## bouton recommencer la partie